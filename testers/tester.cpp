/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdupuis <mdupuis@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/12 11:41:11 by mdupuis           #+#    #+#             */
/*   Updated: 2022/09/12 13:58:40 by mdupuis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector_test.cpp"
#include "map_test.cpp"
#include "stack_test.cpp"
#include <iostream>


int main(int argc, char **argv)
{
	if (argc != 2)
	{
		std::cerr << "Usage: ./tester [container]" << std::endl;
		std::cerr << "Available containers: vector, map, stack" << std::endl;
		return 1;
	}
	std::string container(argv[1]);
	if (container == "vector")
	{
		ft_iterators();
		ft_const_iterators();
		ft_reverse_iterators();
		ft_vector_tests();
	}
	else if (container == "map")
	{
		std::fstream file;
		std::fstream file_std;
		file.open("map_test.txt", std::ios::out | std::ios::trunc);
		file_std.open("map_test_std.txt", std::ios::out | std::ios::trunc);
		ft_iterator(file, file_std);
		ft_map(file ,file_std);
		file.close();
		file_std.close();
	}
	else if (container == "stack")
	{
		std::fstream file_stack;
		file_stack.open("stack_test.txt", std::ios::out | std::ios::trunc);
		std::fstream file_stack_std;
		file_stack_std.open("stack_test_std.txt", std::ios::out | std::ios::trunc);
		ft_stack(file_stack, file_stack_std);
		file_stack.close();
		file_stack_std.close();
	}
	else
	{
		std::cerr << "Usage: ./tester [container]" << std::endl;
		std::cerr << "Available containers: vector, map, stack" << std::endl;
		return 1;
	}
	return 0;
}