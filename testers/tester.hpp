/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdupuis <mdupuis@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/12 13:35:55 by mdupuis           #+#    #+#             */
/*   Updated: 2022/09/12 13:47:53 by mdupuis          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TESTER_HPP
# define TESTER_HPP

#include <fstream>
#include <iostream>
#include "../iterator.hpp"
#include "../vector.hpp"
#include "../map.hpp"
#include "../stack.hpp"
#include "../utils.hpp"
#include "../iterator_traits.hpp"
#include "../reverse_iterator.hpp"
#include "../RedBlackTree.hpp"
#include "vector_test.cpp"
#include "map_test.cpp"
#include "stack_test.cpp"

void ft_iterators(void);
void ft_const_iterators(void);
void ft_reverse_iterators(void);
void ft_vector_tests(void);

void ft_iterator(std::fstream &file, std::fstream &file_std);
void ft_map(std::fstream &file, std::fstream &file_std);

void ft_stack(std::fstream &file, std::fstream &file_std);



#endif